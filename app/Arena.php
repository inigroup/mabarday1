<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Arena extends Model
{
    protected $fillable = ['arena_name', 'location', 'fasilities', 'price', 'type', 'venue_id', 'available'];
    protected $primaryKey = 'arena_id';
    use SoftDeletes;
    protected $dates = ['delete_at'];
}
