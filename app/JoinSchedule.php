<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JoinSchedule extends Model
{
    public function schedule(){
        return $this->hasOne('App\Schedule', 'id', 'id_schedule');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }
}
