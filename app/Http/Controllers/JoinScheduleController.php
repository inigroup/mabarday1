<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Schedule;
use App\JoinSchedule;
use DB;

class JoinScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schedules = Schedule::all();
        // dd($schedules);
        return view('pages/joinschedule', compact('schedules'));
    }

    public function myindex()
    {
        $schedules = Schedule::all();
        // dd($schedules);
        return view('pages/myjoin', compact('schedules'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $joins = new JoinSchedule();
        $joins->id_user = $request->id_user;
        $joins->id_schedule = $request->id_schedule;
        $joins->status = 1;

        $joins->save();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $join = JoinSchedule::where('status', 1)
                            ->with('schedule', 'user')
                            ->get();
        // dd($join);
        $schedules = Schedule::all();
        // dd($schedules);
        return view('pages/myjoin', compact('join', 'schedules'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $join = JoinSchedule::find($id);
        $join->delete();
        return redirect()->back();
    }
}
