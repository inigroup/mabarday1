<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Venue;

class VenueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['menu'] = 2;
        $data['title'] = "Tipe Venue";
        $data['venue'] = Venue::all();
        $data['no'] = 1;
        return view('venue.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['menu'] = 2;
        $data['title'] = "Tambahkan Tipe Venue";
        return view('venue.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'venue_type' => 'required'
        ]);

        $insert = venue::create($request->toArray());
        if($insert == true ){
            $request->session()->flash('success', 'Add venue type success!');
            return redirect()->route('venue.index');
        } else {
            $request->session()->flash('danger', 'Add venue type failed!');
            return redirect()->route('venue.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['menu'] = 2;
        $data['title'] = "Edit Tipe Venue";
        $data['venue'] = Venue::find($id);
        return view('venue.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $request->validate([
            'venue_type' => 'required|max:30',
        ]);

        $update = Venue::find($id)->update($request->toArray());
        if($update) {
            $request->session()->flash('success', 'Add venue type success!');
            return redirect()->route('venue.index');
        } else {
            $request->session()->flash('danger', 'Add venue type failed!');
            return redirect()->route('venue.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $del = Venue::destroy($id);
        return redirect()->route('venue.index');
    }
}
