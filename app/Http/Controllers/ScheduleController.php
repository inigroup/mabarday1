<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Schedule;
use App\Arena;
use DB;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schedules = Schedule::with('arena')->get();
        $arena = Arena::all();
        // dd($schedules);
        return view('pages/schedule', compact('schedules', 'arena'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arena = Arena::all();
        return view('pages/schedule/add', compact('arena'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $schedule = new Schedule();
        $schedule->name = $request->name;
        $schedule->date = $request->date;
        $schedule->time = $request->time;
        $schedule->id_arena = $request->id_arena;
        // dd($request->all());
        $schedule->save();

        //return view('pages/schedule');
        return redirect('/schedule');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $schedules = Schedule::where('id', $id)
                            ->with('arena')
                            ->get();
        // dd($schedules);
        return view('pages/schedule/show',compact('schedules', 'arena'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $schedules = Schedule::where('id', $id)
                            ->with('arena')
                            ->get();
        $arenas = Arena::all();
        // dd($schedules);
        return view('pages/schedule/edit',['schedule' => $schedules], compact('arenas', 'schedules'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('schedules')->where('id', $request->id)->update([
            'name' => $request->name,
            'date' => $request->date,
            'time' => $request->time,
            'id_arena' => $request->id_arena,
        ]);
        return redirect('/schedule');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $schedule = Schedule::find($id);
        $schedule->delete();

        return redirect()->back();
    }
}
