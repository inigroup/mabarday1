<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Arena;
use App\Venue;
use Illuminate\Support\Facades\DB;

class ArenaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = "Arenas";
        $data['menu'] = 1;
        $arenas = DB::table('arenas')
                        ->join('venues', 'arenas.venue_id', '=', 'venues.venue_id')
                        ->get()->toArray();
        //ngereturn array dari query builder laravel
        $data['arena'] = json_decode(json_encode($arenas), true);
        $data['no'] = 1;
        return view('arena.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title'] = "Add New Arenas";
        $data['menu'] = 1;
        $data['venues'] = Venue::all();

        return view('arena.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'arena_name' => 'required',
            'location' => 'required',
            'fasilities' => 'required',
            'price' => 'required|numeric',
            'type' => 'required',
            'venue_id' => 'required'
        ]);

        $insert = Arena::create($request->toArray());

        return redirect()->route('arena.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['title'] = "Edit Arenas";
        $data['menu'] = 1;
        $data['arena'] = Arena::find($id);
        $data['venues'] = Venue::all();

        return view('arena.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title'] = "Edit Arenas";
        $data['menu'] = 1;
        $data['arena'] = Arena::find($id);
        $data['venues'] = Venue::all();

        return view('arena.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $request->validate([
            'arena_name' => 'required',
            'location' => 'required',
            'fasilities' => 'required',
            'price' => 'required|numeric',
            'type' => 'required',
            'venue_id' => 'required'
        ]);

        $update = Arena::find($id)->update($request->toArray());
        return redirect()->route('arena.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        // dd($id); 
       //$data = Arena::where('arena_id',$id)->first();
        //dd($data);
        //$data->delete();
        //return redirect()->route('arena.index')->with('alert-success','Data berhasi dihapus!');
        Arena::destroy($id);
        return redirect()->route('arena.index')->with('alert-success','Data berhasi dihapus!');
    }
}
