<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $fillable = ["name", "date", "time", "id_arena"];

    public function arena(){
        return $this->hasOne('App\Arena', 'arena_id', 'id_arena');
    }
}
