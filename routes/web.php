<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/schedule', function () {
    return view('pages/schedule');
});

Route::group(['prefix' => 'schedule'], function() {
    Route::get('/', 'ScheduleController@index');
    Route::get('/show/{id}', 'ScheduleController@show');
    Route::get('/add', 'ScheduleController@create');
    Route::post('/add', 'ScheduleController@store');
    Route::get('/edit/{id}', 'ScheduleController@edit');
    Route::post('/update', 'ScheduleController@update');
    Route::get('/delete/{key}', 'ScheduleController@destroy');
});

Route::group(['prefix' => 'join'], function() {
    Route::get('/', 'JoinScheduleController@index');
    Route::post('/add', 'JoinScheduleController@store');
    Route::get('/delete/{key}', 'JoinScheduleController@destroy');
});

Route::group(['prefix' => 'myjoin'], function() {
    Route::get('/', 'JoinScheduleController@show');
});

Route::resource('venue', 'VenueController');

Route::resource('arena', 'ArenaController');

Auth::routes();

Route::get('/admin', 'AdminController@index');
Route::get('/user', 'UserController@index');