@extends('layouts.app')

@section('content')

<section class="content row justify-content-center">

    @if ($errors->any())

        @foreach ($errors->all() as $error)
            <p class="alert alert-danger">{{ $error }}</p>
        @endforeach

    @endif

    <div class="card card-secondary card-outline col-md-6">
        <div class="card-header">
            <h3 class="card-title">Form {{$title}} </h3>
        </div>
        <div class="card-body">
            <form action="{{ route('venue.store') }}" method="POST">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <p>Venue Type</p>
                            <input type="text" class="form-control" required name="venue_type" value="{{ old('venue_type') }}" >
                        </div>

                    </div>

                </div>
                <input type="submit" class="btn btn-primary float-right" value="Add New Venue">
            </form>
        </div>
    </div>
</section>

@endsection
