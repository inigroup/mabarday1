@extends('layouts.app')

@section('content')


<section class="content row justify-content-center">
    <div class="card card-secondary card-outline col-md-8">
        <div class="card-header">
            @include('layouts.navbar')
            @include('layouts.navbaradmin')
        </div>
        <div class="card-body">
            <h3 class="card-title">
                <a href="{{ route('venue.create') }}" class="btn btn-primary">
                    <i class="fas fa-basketball-ball"></i>
                    Add New
                </a>
            </h3>
            <table class="table table-sm" id="myTable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Tipe Venue</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($venue as $row)
                    <tr>
                        <td>{{ $no++}}</td>
                        <td>{{ $row['venue_type'] }}</td>
                        <td>
                            <a href="{{ route('venue.edit',  ['venue_id' => $row['venue_id']]) }}" class="btn btn-success btn-circle btn-sm">
                                <i class="fa fa-cog"></i>
                            </a>
                            <a data-id="{{$row->venue_id}}" class="btn btn-danger btn-circle btn-sm">
                                <i class="fa fa-trash" aria-hidden="true" style="color:white"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>

            </table>
        </div>
    </div>
</section>

@endsection


@push('scripts')
<script>
    $(".delete-btn").click(function(){
        alert('tes')
        let id = $(this).attr('data-id');
        if(confirm("Apa anda yakin akan menghapus? ")) {
            $.ajax({
                url : "{{url('/')}}/venue/"+id,
                method : "POST",
                data : {
                    _token : "{{csrf_token()}}",
                    _method : "DELETE",
                }
            })
            .then(function(data){
                location.reload();
            });
        }
    })
</script>
@endpush
