@extends('layouts.app')

@section('content')
<script data-search-pseudo-elements defer src="https://use.fontawesome.com/releases/latest/js/all.js" integrity="sha384-L469/ELG4Bg9sDQbl0hvjMq8pOcqFgkSpwhwnslzvVVGpDjYJ6wJJyYjvG3u8XW7" crossorigin="anonymous"></script>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    @include('layouts.navbar')
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <center>
                        <h1>WELCOME TO MABARDAY!</h1><hr>
                        <h5>Di Mabarday kamu bisa :</h5>
                    </center>
                    <br>
                    <div class="row">
                        <div class="col-md-4 text-center">
                            <i class="fa fa-group fa-lg" style="color:blueviolet; font-size:100px"></i>
                        </div>
                        <div class="col-md-8">
                            <h2>Ngajak orang main bareng kamu!</h2>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="row">
                        <div class="col-md-4 text-center">
                            <i class="fa fa-tachometer fa-lg" style="color:blueviolet; font-size:100px"></i>
                        </div>
                        <div class="col-md-8">
                            <h2>Ikut main bareng sama orang lain</h2>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="row">
                        <div class="col-md-4 text-center">
                            <i class="fa fa-edit fa-lg" style="color:blueviolet; font-size:100px"></i>
                        </div>
                        <div class="col-md-8">
                            <h2>Dan lihat jadwal main barengmu!</h2>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
