@extends('layouts.app');
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    @include('layouts.navbar')
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <a href="/schedule/add" class="btn btn-primary">
                        <i class="fas fa-calendar-plus"></i>
                        &nbsp;Add Schedule
                    </a>
                    <br><hr>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Schedule</th>
                                <th>Date</th>
                                <!-- <th>Time</th>
                                <th>Arena</th> -->
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($schedules as $key => $schedule)
                            <tr>
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $schedule->name }}</td>
                                <td>{{ $schedule->date }}</td>
                                <!-- <td>{{ $schedule->time }}</td>
                                <td>{{ $schedule->arena['arena_name'] }}</td> -->
                                <td>
                                    <a href="<?= url('/schedule/show/' . $schedule->id) ?>" class="btn btn-success btn-circle btn-sm">
                                        <i class="fas fa-eye"></i>
                                    </a>
                                    <a href="<?= url('/schedule/edit/' . $schedule->id) ?>" class="btn btn-warning btn-circle btn-sm">
                                        <i class="fas fa-edit"></i>
                                    </a>
                                    <a href="<?= url('/schedule/delete/' . $schedule->id) ?>" class="btn btn-danger btn-circle btn-sm">
                                        <i class="fas fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
