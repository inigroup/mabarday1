@extends('layouts.app');
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    @include('layouts.navbar')
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @foreach($schedules as $schedule)
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="card border-left-primary shadow py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                Tanggal : {{ $schedule->date }} || Jam : {{ $schedule->time }}
                                            </div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                                {{ $schedule->name }}
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <form action="{{ url('/join/add') }}" method="POST">
                                            {{ csrf_field() }}
                                                <input type="hidden" name="id_user" value="1">
                                                <input type="hidden" name="id_schedule" value="{{ $schedule->id }}">
                                                <input type="submit" value="Join" class="btn btn-success">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
