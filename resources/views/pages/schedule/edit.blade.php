@extends('layouts.app');
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Add New Schedule</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @foreach($schedule as $schedules)
                    <form action="<?= url('/schedule/update') ?>" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $schedules->id }}">
                        <input type="text" value="{{ $schedules->name }}" name="name" id="name" class="form-control form-control-user">
                        <hr>
                        <input type="date" name="date" value="{{ $schedules->date }}" class="form-control form-control-user">
                        <hr>
                        <input type="time" name="time" value="{{ $schedules->time }}" class="form-control form-control-user">
                        <hr>
                        <select name="id_arena" id="venue" class="form-control form-control-user">
                            <option value="{{ $schedules->id_arena }}">{{ $schedules->arena['arena_name'] }}</option>
                            @foreach($arenas as $key => $data)
                                <option value="{{ $data->arena_id }}">{{ $data->arena_name }}</option>
                            @endforeach
                        </select>
                        <hr>
                        <input type="submit" value="Update Schedule" class="btn btn-primary">
                    </form>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
