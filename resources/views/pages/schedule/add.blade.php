@extends('layouts.app');
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Add New Schedule</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form action="<?= url('/schedule/add') ?>" method="post">
                        {{ csrf_field() }}
                        <input type="text" name="name" id="name" class="form-control form-control-user" placeholder="Nama Jadwal">
                        <hr>
                        <input type="date" name="date" data-date-format="YYYY MM DD" value="2019-01-01" class="form-control form-control-user">
                        <hr>
                        <input type="time" name="time" min="8:00" max="18:00" value="08:00" class="form-control form-control-user">
                        <hr>
                        <select name="id_arena" id="venue" class="form-control form-control-user">
                            <option selected disabled>Pilih Arena</option>
                            @foreach($arena as $key => $data)
                                <option value="{{ $data->arena_id }}">{{ $data->arena_name }}</option>
                            @endforeach
                        </select>
                        <hr>
                        <input type="submit" value="Add New Schedule" class="btn btn-primary">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
