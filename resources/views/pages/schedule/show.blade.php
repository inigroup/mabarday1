@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Detail of Schedule
                </div>

                <div class="card-body">
                @foreach($schedules as $key => $schedule)
                        <table class="table">
                            <tr>
                                <td>Nama</td>
                                <td>:</td>
                                <td>{{ $schedule->name }}</td>
                            </tr>
                            <tr>
                                <td>Tanggal</td>
                                <td>:</td>
                                <td>{{ $schedule->date }}</td>
                            </tr>
                            <tr>
                                <td>Waktu</td>
                                <td>:</td>
                                <td>{{ $schedule->time }}</td>
                            </tr>
                            <tr>
                                <td>Arena</td>
                                <td>:</td>
                                <td>{{ $schedule->arena['arena_name'] }}</td>
                            </tr>
                        </table>
                    @endforeach
                    <hr>
                    <a href="/schedule" class="btn btn-primary float-right">Back</a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
