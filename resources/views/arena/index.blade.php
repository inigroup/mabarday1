@extends('layouts.app')

@section('content')

<section class="content row justify-content-center">
    <div class="card card-secondary card-outline col-md-8">
        <div class="card-header">
            @include('layouts.navbar')
            @include('layouts.navbaradmin')
        </div>
        <div class="card-body">
            <a href="{{ route('arena.create') }}" class="btn btn-primary">
                <i class="fas fa-basketball-ball"></i>
                Add New
            </a><hr>
            <table class="table table-sm" id="myTable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Arena</th>
                        <th>Lokasi</th>
                        <!-- <th>Fasilitas</th>
                        <th>Harga</th>
                        <th>Tipe</th>
                        <th>Venue</th>-->
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($arena as $row)
                    <tr>
                        <td>{{ $no++}}</td>
                        <td>{{ $row['arena_name'] }}</td>
                        <td>{{ $row['location'] }}</td>
                        <!-- <td>{{ $row['fasilities'] }}</td>
                        <td>{{ number_format($row['price']) }}</td>
                        <td>{{ $row['type'] }}</td>
                        <td>{{ $row['venue_type'] }}</td> -->
                        <td>
                            <a href="{{ route('arena.show',  ['arena_id' => $row['arena_id']]) }}" class="btn btn-sm btn-success btn-circle">
                                <i class="fa fa-eye"></i>
                            </a>
                            <a href="{{ route('arena.edit',  ['arena_id' => $row['arena_id']]) }}" class="btn btn-sm btn-warning btn-circle">
                                <i class="fa fa-edit"></i>
                            </a>
                            <!--<a data-id="{{$row['arena_id']}}" class="btn btn-sm btn-danger delete-btn btn-circle">
                                <i class="fa fa-trash" style="color:white"></i>
                            </a>-->
                            <form style="display: inline;" action="{{ route('arena.destroy', ['arena_id' => $row['arena_id']]) }}" method="post">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                <button class="btn btn-sm btn-danger delete-btn btn-circle" type="submit" onclick="return confirm('Yakin ingin menghapus data?')">
                                <i class="fa fa-trash" style="color:white"></i>
                            </form>
                            </button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>

            </table>
        </div>
    </div>
</section>


@endsection

