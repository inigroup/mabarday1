@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Detail of Arena
                </div>

                <div class="card-body">
                {{ method_field('PUT') }}
                        <table class="table">
                            <tr>
                                <td>Nama</td>
                                <td>:</td>
                                <td>{{ $arena->arena_name }}</td>
                            </tr>
                            <tr>
                                <td>Lokasi</td>
                                <td>:</td>
                                <td>{{ $arena->location }}</td>
                            </tr>
                            <tr>
                                <td>Fasilitas</td>
                                <td>:</td>
                                <td>{{ $arena->fasilities }}</td>
                            </tr>
                            <tr>
                                <td>Harga</td>
                                <td>:</td>
                                <td>{{ $arena->price }}</td>
                            </tr>
                            <tr>
                                <td>Type</td>
                                <td>:</td>
                                <td>{{ $arena->type }}</td>
                            </tr>
                            <!-- <tr>
                                <td>Venue</td>
                                <td>:</td>
                                <td>{{ $arena->venue_type }}</td>
                            </tr> -->
                        </table>
                    <hr>
                    <a href="/arena" class="btn btn-primary float-right">Back</a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
