@extends('layouts.app')

@section('content')

<section class="content col-md-6 row justify-content-center">
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <p class="alert alert-danger">{{ $error }}</p>
        @endforeach
    @endif

    <div class="card card-secondary card-outline">
        <div class="card-header">
            <h3 class="card-title">Form {{$title}} </h3>
        </div>
        <div class="card-body">
            <form action="{{ route('arena.update', ['arena_id' => $arena->arena_id]) }}" method="POST">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <p>Nama Arena</p>
                            <input type="text" class="form-control" required name="arena_name" value="{{ $arena->arena_name }}" >
                        </div>
                        <div class="form-group">
                            <p>Lokasi</p>
                            <input type="text" class="form-control" required name="location" value="{{ $arena->location }}">
                        </div>
                        <div class="form-group">
                            <p>Fasilitas</p>
                            <input type="text" class="form-control" required name="fasilities" value="{{ $arena->fasilities }}">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <p>Harga</p>
                            <input type="number" class="form-control" required name="price" value="{{ $arena->price }}">
                        </div>
                        <div class="form-group">
                            <p>Tipe</p>
                            <select name="type" class="form-control">
                                <option>- Select one -</option>
                                <option value="indoor" {{ ($arena->type == 'indoor' ? "selected":"") }}>Indoor</option>
                                <option value="outdoor" {{ ($arena->type == 'outdoor' ? "selected":"") }}>Outdoor</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <p>Venue</p>
                            <select name="venue_id" class="form-control">
                                <option value="">- Pilih Satu -</option>
                                @foreach($venues as $venue)
                                    <option value="{{$venue->venue_id}}" {{($arena->venue_id == $venue->venue_id ? 'selected' : '')}} >{{$venue->venue_type}}</option>
                                 @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <input type="submit">
            </form>
        </div>
    </div>
</section>

@endsection
